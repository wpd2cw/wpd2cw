# Simple Web Shop

This shows a shop using the Java Servlet framework and Google Guice as the dependency injection framework.

This is a Simple Web Store that allows users to register, login and view their Wish List. From the store a user can add or remove products from their Wish List. They can also view products on the store, and see a template review system. 

The code is licensed under the GPL Version 3.

##Requirements
To run it you need Java 8, Maven and (optionally) Intellij for which there is a project set up.

##Using Git
The repository is stored in Git. This initial version is on the 'master' branch.
To get started, clone the repository.
Get started
You must have Java and Maven installed and can run Maven with the <code>mvn</code> command at a prompt. To check this get up a command prompt and type <code>mvn --version</code> (note two hyphens). You should see a version number and other stuff about the Java version.
By default you are running from the address http://localhost:9000 on the development server. All the paths below are relative to this, which may differ for you if you change it.

Open a web browser and go to: 'http://localhost:9000/' You will see the index page with a link 'Enter Store'. Clicking this link takes you to the shop home. Click wishlist to log in to the site. Once logged in you can add products to the wishlist.

##Technology stack in the starter
The starter uses Jetty to run an embedded web server.
The main class is Runner.