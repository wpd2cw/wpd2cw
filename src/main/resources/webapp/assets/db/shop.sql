CREATE TABLE IF NOT EXISTS sku (
  skuId VARCHAR(255) PRIMARY KEY,
  title VARCHAR(255),
  shortDescription VARCHAR(4096),
  longDescription VARCHAR(4096),
  thumbnail VARCHAR(255),
  image VARCHAR(255),
  unitPrice VARCHAR(8),
  addDate TIMESTAMP
);

CREATE TABLE IF NOT EXISTS shop (
  id int AUTO_INCREMENT PRIMARY KEY,
  shopName VARCHAR(255),
  skuId  VARCHAR(255),
  FOREIGN KEY(skuId) REFERENCES sku(skuId)
);

CREATE TABLE IF NOT EXISTS wishlist (
  id int AUTO_INCREMENT PRIMARY KEY,
  userId VARCHAR(255), // can be JSESSIONID cookie value
  skuId VARCHAR(255),
  quantity INT
);

CREATE TABLE IF NOT EXISTS messages (
  id int AUTO_INCREMENT PRIMARY KEY,
  userId VARCHAR(255),
  message VARCHAR(4096),
  msg_date TIMESTAMP,
  reply VARCHAR(4096),
  reply_date TIMESTAMP
);

CREATE INDEX IF NOT EXISTS shop_index ON shop(shopName);
CREATE INDEX IF NOT EXISTS wishlist_index ON wishlist(userId);
CREATE INDEX IF NOT EXISTS user_index ON messages(userId);

