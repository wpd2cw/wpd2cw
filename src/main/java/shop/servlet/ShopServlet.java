package shop.servlet;

import shop.model.Shop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ShopServlet extends BaseServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(ShopServlet.class);

    private static final String STORE_TEMPLATE = "shop.mustache";
    private static final long serialVersionUID = -7417844579010073210L;

    private final Shop shop;

    public ShopServlet(Shop shop) {
        super();
        this.shop = shop;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String,Object> map = new HashMap<>();
        map.put("shop", shop);
        showView(response, STORE_TEMPLATE, map);
        cache(response);
    }
}
