package shop.servlet;

import com.google.common.net.MediaType;
import util.MustacheRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MustacheServlet extends BaseServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(MustacheServlet.class);

    private static final String MUSTACHE_EXTENSION = ".mustache";
    private static final long serialVersionUID = 1421709970541323735L;

    private final MustacheRenderer render;

    public MustacheServlet() {
        render = new MustacheRenderer();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (uri.startsWith("/")) {
            uri = uri.substring(1);
        }
        String template = changeExtension(uri);
        String html = render.render(template, new Object());
        issue(MediaType.HTML_UTF_8, HttpServletResponse.SC_OK, html, response);
    }

    private static String changeExtension(String in) {
        int index = in.lastIndexOf('.');
        return (index == -1) ? in + MUSTACHE_EXTENSION : in.substring(0, index) + MUSTACHE_EXTENSION;
    }

}

