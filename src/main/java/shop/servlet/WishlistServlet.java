package shop.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import shop.model.Shop;
import shop.model.Wishlist;
import shop.servlet.helper.WishlistLoader;

public class WishlistServlet extends BaseServlet {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(WishlistServlet.class);

    private static final String WISHLIST_TEMPLATE = "wishlist.mustache";
    private static final long serialVersionUID = -8763838750999908754L;

    private final Shop shop;

    public WishlistServlet(Shop shop) {
        this.shop = shop;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Wishlist wishlist = new WishlistLoader().load(request);
        Map<String,Object> map = new HashMap<>();
        map.put("lines", wishlist.orderLines(shop));
        map.put("wishlist", wishlist);
        showView(response, WISHLIST_TEMPLATE,  map);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String skuId = request.getParameter("skuId");
        String quantityString = request.getParameter("quantity");
        LOG.info("Post to the wishlist, ID: " + request.getParameter("skuId"));
        if (skuId != null) {
            Wishlist wishlist= new WishlistLoader().load(request);
            wishlist.add(skuId);
            new WishlistLoader().save(request, wishlist);
        }
        response.sendRedirect(response.encodeRedirectURL("/login" ));
    }
    @Override
    protected void doDelete(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
        String skuId=request.getParameter("skuId");
        if(skuId!=null){
            Wishlist wishlist=new WishlistLoader().load(request);
            wishlist.clear(skuId);
            new WishlistLoader().save(request,wishlist);
        }
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

}
