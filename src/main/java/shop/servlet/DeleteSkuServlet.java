package shop.servlet;

import shop.model.Wishlist;
import shop.model.Shop;
import shop.servlet.helper.WishlistLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteSkuServlet extends BaseServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(DeleteSkuServlet.class);
    private static final long serialVersionUID = 1848588541476220169L;

    private final Shop shop;

    public DeleteSkuServlet(Shop shop) {
        this.shop = shop;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String skuId = request.getParameter("skuId");
        if (skuId != null) {
            Wishlist wishlist = new WishlistLoader().load(request);
            wishlist.clear(skuId);
            new WishlistLoader().save(request, wishlist);
        }
        response.sendRedirect(response.encodeRedirectURL("/shop/wishlist"));
    }
}
