package shop.servlet;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.net.MediaType;
import util.MustacheRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class BaseServlet extends HttpServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(BaseServlet.class);
    private static final long serialVersionUID = -8591774661794962818L;

    private final MustacheRenderer mustache;
    private static final int CACHE_SECONDS = 60;

    BaseServlet() {
        mustache = new MustacheRenderer();
    }

    private void issue(MediaType mimeType, int returnCode, byte[] output, HttpServletResponse response) throws IOException {
        response.setContentType(mimeType.toString());
        response.setStatus(returnCode);
        response.getOutputStream().write(output);
    }

    void issue(MediaType mimeType, int returnCode, String output, HttpServletResponse response) throws IOException {
        issue(mimeType, returnCode, output.getBytes(Charsets.UTF_8), response);
    }

    void cache(HttpServletResponse response) {
        Preconditions.checkArgument(CACHE_SECONDS > 0);
        response.setHeader("Pragma", "Public");
        response.setHeader("Cache-Control", "public, no-transform, max-age="+CACHE_SECONDS);
    }

    void showView(HttpServletResponse response, String templateName, Object model) throws IOException {
        String html = mustache.render(templateName, model);
        issue(MediaType.HTML_UTF_8, HttpServletResponse.SC_OK, html, response);
    }

    String user(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return null;
        }
        return (String)session.getAttribute("user");
    }

    String redirect(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String redirect = (String)session.getAttribute("redirect");
        return (redirect == null) ? "/shop/wishlist" : redirect;
    }

    Map<String,Object> baseMap(HttpServletRequest request) {
        String user = user(request);
        String showName = (user == null) ? "<not logged in>" : user;
        Map<String,Object> map = new HashMap<>();
        if (user!= null) map.put("user", user);
        map.put("userName", showName);
        map.put("currentURI", request.getRequestURI());
        return map;
    }
}
