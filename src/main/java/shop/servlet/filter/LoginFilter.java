package shop.servlet.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

public class LoginFilter implements Filter {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(LoginFilter.class);

    private final Set<String> loginRequired;

    public LoginFilter(Set<String> loginRequired) {
        this.loginRequired = loginRequired;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)req;
        String uri = request.getRequestURI();
        if (loginRequired.contains(uri)) {
            // we need to be logged in to access this URL
            String user = user(request);
            if (user == null){
                HttpServletResponse response = (HttpServletResponse)resp;
                HttpSession session = request.getSession(true);
                session.setAttribute("redirect", uri);
                response.sendRedirect("/login");
                LOG.info("LOGIN filter redirect from /login to " + uri);
                return;
            }
        }
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy() {}

    protected String user(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return null;
        }
        return (String)session.getAttribute("user");
    }
}
