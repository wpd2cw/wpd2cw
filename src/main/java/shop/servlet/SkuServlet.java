package shop.servlet;

import com.google.common.net.MediaType;
import shop.model.Shop;
import shop.model.Wishlist;
import shop.model.Sku;
import shop.servlet.helper.WishlistLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class SkuServlet extends BaseServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(SkuServlet.class);

    private static final String SKU_TEMPLATE = "sku.mustache";
    private static final long serialVersionUID = -4661806306632338332L;

    private final Shop shop;

    public SkuServlet(Shop shop) {
        this.shop = shop;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String skuId = skuId(request);
        Sku sku = shop.getSku(skuId);
        if (sku == null) {
            issue(MediaType.PLAIN_TEXT_UTF_8, HttpServletResponse.SC_BAD_REQUEST, "Can't find SKU with the id " + skuId, response);
        } else {
            Wishlist wishlist = new WishlistLoader().load(request);
            Map<String,Object> map = new HashMap<>();
            map.put("item", sku);
            map.put("wishlist", wishlist);
            showView(response, SKU_TEMPLATE, map);
        }
    }

    private String skuId(HttpServletRequest request) {
        String uri = request.getRequestURI();
        String[] sub = uri.split("/");
        return sub[sub.length-1];
    }

}
