package shop.servlet.helper;

import shop.model.Shop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

public class ShopLoader {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(ShopLoader.class);

    public ShopLoader() {}

    public Shop load()  {
        try {
            return Shop.fromJSONString(loadString(getClass().getResource("/webapp/assets/json/shop.json")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String loadString(URL url) throws IOException {
        return new Scanner(url.openStream()).useDelimiter("\\Z").next();
    }
}