package shop.servlet.helper;

import lombok.NonNull;
import shop.model.Wishlist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class WishlistLoader {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(WishlistLoader.class);

    private static final String WISHLIST_PROPERTY = "wishlist";

    public WishlistLoader() {}

    public Wishlist load(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        Wishlist wishlist = (session == null) ? null : (Wishlist) session.getAttribute(WISHLIST_PROPERTY);
        return (wishlist == null) ? emptyWishlist() : wishlist;
    }

    public void save(HttpServletRequest request, @NonNull Wishlist wishlist) {
        HttpSession session = request.getSession(true);
        if (session != null) {
            session.setAttribute(WISHLIST_PROPERTY, wishlist);
        }
    }

    private Wishlist emptyWishlist() {
        return new Wishlist();
    }
}
