package shop;

import auth.MemUser;
import shop.model.Shop;
import shop.model.db.H2Shop;
import shop.servlet.*;
import shop.servlet.filter.LoginFilter;
import shop.servlet.helper.ShopLoader;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.DispatcherType;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;


public class Runner {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(Runner.class);

    private static final int PORT = 9000;

    private final Shop shop;

    private Runner() {
        shop = new ShopLoader().load();
    }

    private void start() throws Exception {
        Server server = new Server(PORT);

        ServletContextHandler handler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);
        handler.setContextPath("/");
        handler.setInitParameter("org.eclipse.jetty.servlet.Default." + "resourceBase", "src/main/resources/webapp");

        LoginFilter loginFilter = new LoginFilter(loginRequired());
        handler.addFilter(new FilterHolder(loginFilter), "/*", EnumSet.allOf(DispatcherType.class));

        handler.addServlet(new ServletHolder(new LoginServlet(new MemUser())), "/login");
        handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout");

        ShopServlet ss = new ShopServlet(shop);
        handler.addServlet(new ServletHolder(ss), "/shop");

        SkuServlet skuServlet = new SkuServlet(shop);
        handler.addServlet(new ServletHolder(skuServlet), "/shop/sku/*");

        WishlistServlet cartServlet = new WishlistServlet(shop);
        handler.addServlet(new ServletHolder(cartServlet), "/shop/wishlist");
       // handler.addServlet(new ServletHolder(cartServlet), "/shop/cart/pay");

        DeleteSkuServlet deleteSkuServlet = new DeleteSkuServlet(shop);
        handler.addServlet(new ServletHolder(deleteSkuServlet), "/shop/cart/deleteSku");

        MustacheServlet ms = new MustacheServlet();
        handler.addServlet(new ServletHolder(ms), "*.html");

        H2Shop h2Shop = new H2Shop(H2Shop.MEM_DB);
        handler.addServlet(new ServletHolder(new MessagesServlet(h2Shop)), "/messages");

        DefaultServlet ds = new DefaultServlet();
        handler.addServlet(new ServletHolder(ds), "/");

        server.start();
        LOG.info("Server started, will run until terminated");
        server.join();
    }

    private Set<String> loginRequired() {
        Set<String> set = new HashSet<>();
        set.add("/messages");
        return set;
    }

    public static void main(String[] args) {
        try {
            LOG.info("starting");
            Locale.setDefault(Locale.UK);
            new Runner().start();
        } catch (Exception e) {
            LOG.error("Unexpected error running shop: " + e.getMessage());
        }
    }
}
