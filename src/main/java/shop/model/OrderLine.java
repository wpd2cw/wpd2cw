package shop.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigDecimal;
import java.text.NumberFormat;

@Data
public class OrderLine {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(OrderLine.class);

    private final Sku sku;
    private final int quantity;


    public OrderLine(String skuId, int quantity, Shop shop) {
        this.sku = shop.getSku(skuId);
        this.quantity = quantity;
    }

    public BigDecimal cost() {
        return getUnitPrice().multiply(new BigDecimal(quantity));
    }

    public String getCostString() {
        return  NumberFormat.getCurrencyInstance().format(cost());
    }

    public BigDecimal getUnitPrice() {
        return (sku == null) ? new BigDecimal("0") : sku.getUnitPrice();
    }

    public String getUnitPriceString() {
        return NumberFormat.getCurrencyInstance().format(getUnitPrice());
    }

    public String getTitle() {
        return (sku == null) ? "" : sku.getTitle();
    }

    public String getThumbUrl() {
        return (sku == null) ? "" : sku.getThumbnail();
    }
}
