package shop.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.Charsets;
import lombok.Data;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("unused")
public class Shop implements Serializable {
    static final Logger LOG = LoggerFactory.getLogger(Shop.class);
    private static final long serialVersionUID = -4478190721183244953L;

    private String shopName;
    private String storeCurrency;
    private Set<Sku> skus;

    public static Shop fromBytes(@NonNull byte[] bytes) {
        try {
            return new ShopMapper().readValue(bytes, Shop.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public static Shop fromJSONString(@NonNull String s) {
        return fromBytes(s.getBytes(Charsets.UTF_8));
    }

    public Shop() {
        storeCurrency = "GBP";
        skus = new LinkedHashSet<>();
    }

    public Shop(@NonNull String storeName) {
        this();
        this.shopName = storeName;
    }

    public Shop(@NonNull Shop shop) {
        this.shopName = shop.shopName;
        this.storeCurrency = shop.storeCurrency;
        this.skus = new LinkedHashSet<>(shop.skus);
    }

    public Shop addSku(@NonNull Sku sku) {
        skus.add(sku);
        return this;
    }


    public Sku getSku(@NonNull String id) {
        for (Sku sku : skus) {
            if (id.equals(sku.getId())) {
                return sku;
            }
        }
        return null;
    }

    public String toJSONString() {
        try {
            return new ShopMapper().writeValueAsString(this);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
