package shop.model.db;

import lombok.NonNull;
import shop.model.*;
import util.H2Base;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class H2Shop extends H2Base implements AutoCloseable {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(H2Shop.class);

    public static final String MEM_DB = "jdbc:h2:mem:shop";
    public static final String FILE_DB = "jdbc:h2:~/shop";


    /**
     * Create a new link to the shop database
     * @param dbFile  The type of backing to the database.
     * @throws H2ShopException if there is a database problem.
     */
    public H2Shop(String dbFile) {
        super(dbFile);
        try {
            loadResource("/webapp/assets/db/shop.sql");
        } catch (Exception e) {
            LOG.error("Can't find database driver: " + e.getMessage());
            throw new H2ShopException(e);
        }
    }

    public Wishlist loadWishlist(@NonNull final String cookieValue) {
        Wishlist wishlist = new Wishlist();
        try (PreparedStatement ps = prepare("SELECT skuId FROM wishlist WHERE userId = ?")) {
            ps.setString(1, cookieValue);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                wishlist.add(rs.getString(1));
            }
            return wishlist;
        } catch (SQLException e) {
            throw new H2ShopException(e);
        }
    }


    public void saveWishlist(@NonNull final String cookieValue, @NonNull Wishlist wishlist) {
        try (PreparedStatement ps = prepare("DELETE FROM wishlist WHERE userId = ?");
            PreparedStatement ps2 = prepare("INSERT INTO wishlist (userId, skuId) VALUES (?,?)")) {
            ps.setString(1, cookieValue);
            ps.execute();

           // setAutoCommit(false);
            for (Map.Entry<String,Integer> entry : wishlist.getEntries()) {
                ps2.setString(1, cookieValue);
                ps2.setString(2, entry.getKey());
                ps2.execute();
            }
          //  commit();
          //  setAutoCommit(true);
        } catch (SQLException e) {
            throw new H2ShopException(e);
        }
    }

    /**
     * Given a shop, shop it in the database.
     * The previous shop with this name (if any) will be deleted
     * @param shop The shop to shop
     * @throws H2ShopException if there is a problem with the database
     */
    public void storeShop(@NonNull Shop shop) {
        String shopName = shop.getShopName();
        try (PreparedStatement ps = prepare("DELETE FROM shop WHERE shopName = ?");
             PreparedStatement ps2 = prepare("INSERT INTO shop (shopName, skuId) VALUES (?,?)")) {
            ps.setString(1, shopName);
            ps.execute();

           //setAutoCommit(false);
            for (Sku sku : shop.getSkus()) {
                saveSku(sku);
                ps2.setString(1, shopName);
                ps2.setString(2, sku.getId());
                ps2.execute();
            }
         //   commit();
         //   setAutoCommit(true);
        } catch (SQLException e) {
            throw new H2ShopException(e);
        }
    }

    /**
     * Given a shop name, load the shop into memory.
     * @param shopName  The name of the shop
     * @return The stored shop.  An empty shop if nothing was stored
     * @throws H2ShopException if there is a database problem
     */
    public Shop loadShop(String shopName) {
        Shop shop = new Shop(shopName);
        try (PreparedStatement ps = prepare("SELECT skuId FROM shop WHERE shopName = ?")) {
            ps.setString(1, shopName);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String skuId = rs.getString(1);
                Sku sku = loadSku(skuId);
                shop.addSku(sku);
            }
            return shop;
        } catch (SQLException e) {
            throw new H2ShopException(e);
        }

    }

    public List<Message> messagesFor(String userId) {
        List<Message> out = new ArrayList<>();
        try (PreparedStatement ps = prepare("SELECT userId, message, msg_date, reply, reply_date FROM messages WHERE userId=? ORDER BY msg_date DESC")){
            ps.setString(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Message msg = new Message()
                        .setUserId(rs.getString(1))
                        .setMessage(rs.getString(2))
                        .setMessageDate(rs.getTimestamp(3))
                        .setReply(rs.getString(4))
                        .setReplyDate(rs.getTimestamp(5));
                out.add(msg);
            }
        } catch (SQLException e) {
            throw new H2ShopException(e);
        }
        return out;
    }

    public void addMessage(String userId, String message) {
        long now = new java.util.Date().getTime();

        try (PreparedStatement ps = prepare("INSERT INTO messages (userId, message, msg_date, reply, reply_date) VALUES (?,?,?,?,?)")) {
            ps.setString(1, userId);
            ps.setString(2, message);
            ps.setTimestamp(3, new Timestamp(now));
            ps.setString(4, "Thank you for your message.  We will reply as soon as possible.");
            ps.setTimestamp(5, new Timestamp(now + 1000L));
            ps.execute();
        } catch (SQLException e)  {
            throw new H2ShopException(e);
        }
    }

    private void loadResource(String name) {
        try {
            String cmd = new Scanner(getClass().getResource(name).openStream()).useDelimiter("\\Z").next();
            try (PreparedStatement ps = prepare(cmd)) {
                ps.execute();
            } catch (Exception e) {
                throw new H2ShopException(e);
            }
        } catch (IOException e) {
            throw new H2ShopException("Can't open " + name + " to load db commands: " + e.getMessage());
        }
    }


    public void saveSku(Sku sku){
        long now = new java.util.Date().getTime();
        String pss = "INSERT INTO sku (skuId, title, unitPrice, thumbnail, image, shortDescription, longDescription, addDate) " +
                "VALUES (?,?,?,?,?,?,?)";
        try (PreparedStatement ps = prepare(pss)) {
            ps.setString(1, sku.getId());
            ps.setString(2, sku.getTitle());
            ps.setString(3, sku.getShortDescription());
            ps.setString(4, sku.getLongDescription());
            ps.setString(5, sku.getThumbnail());
            ps.setString(6, sku.getImage());
            ps.setString(7, sku.getUnitPrice().toString());
            ps.setTimestamp(8, new Timestamp(now));
            ps.execute();
        } catch (SQLException e) {
            throw new H2ShopException(e);
        }
    }


    public Sku loadSku(String skuId) {
        final String s = "SELECT title, unitPrice, thumbnail, image, shortDescription, longDescription, addDate FROM sku WHERE skuId = ?";
        try (PreparedStatement ps = prepare(s)) {
            ps.setString(1, skuId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Sku(skuId)
                        .setTitle(rs.getString(1))
                        .setShortDescription(rs.getString(2))
                        .setLongDescription(rs.getString(3))
                        .setThumbnail(rs.getString(4))
                        .setImage(rs.getString(5))
                        .setUnitPrice(new BigDecimal(rs.getString(6)))
                        .setAddDate(rs.getTimestamp(7));
            }
            return null;
        } catch (SQLException e) {
            throw new H2ShopException(e);
        }
    }

    public class H2ShopException extends RuntimeException {
        H2ShopException(Exception e) {
            super(e);
        }
        H2ShopException(String s) {
            super(s);
        }
    }
}

