package shop.model.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;


public class SessionId {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(SessionId.class);

    private final String COOKIE_NAME = "JSESSIONID";

    private final HttpServletRequest request;

    public SessionId(HttpServletRequest request) {
        this.request = request;
    }

    public String getSessionId() {
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(COOKIE_NAME)) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }
}
