package shop.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Accessors(chain=true)
public class Sku implements Serializable {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(Sku.class);
    private static final long serialVersionUID = -4119412393370563146L;

    private String id;
    private String title;
    private String shortDescription;
    private String longDescription;
    private String thumbnail;
    private String image;
    private BigDecimal unitPrice;
    private Timestamp addDate;
    private List<String> tags;

    public Sku() {
        this("");
    }

    public Sku(@NonNull String id) {
        this.id = id;
    }

    public String unitPriceString() {
        return NumberFormat.getCurrencyInstance().format(getUnitPrice());
    }

    public String toJsonString() {
        try {
            return new ShopMapper().writeValueAsString(this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Sku) {
            Sku oSku = (Sku)o;
            return id.equals(oSku.id);
        }
        return false;
    }

}
