package shop.model;

import lombok.Data;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;


@Data
public class Wishlist implements Serializable {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(Wishlist.class);
    private static final long serialVersionUID = -746135577060516953L;

    private final Map<String, Integer> entries;

    public Wishlist() {
        entries = new LinkedHashMap<>();
    }

    @SuppressWarnings({"unused"})
    public int getCount() {
        int count = 0;
        for (Map.Entry<String, Integer> entry : entries.entrySet()) {
            count += entry.getValue();
        }
        return count;
    }

    public List<OrderLine> orderLines(Shop shop) {
        List<OrderLine> out = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : entries.entrySet()) {
            OrderLine line = new OrderLine(entry.getKey(), entry.getValue(), shop);
            out.add(line);
        }
        return out;
    }

    public BigDecimal cost(Shop shop) {
        List<OrderLine> lines = orderLines(shop);
        BigDecimal cur = new BigDecimal("0");
        for (OrderLine line : lines) {
            cur = cur.add(line.cost());
        }
        return cur;
    }

    /*
    public String getCostString(Shop shop) {
        return NumberFormat.getCurrencyInstance().format(cost(shop));
    }
    */

    @SuppressWarnings({"unused"})
    public boolean hasContents() {
        return entries.size() > 0;
    }

    public Set<Map.Entry<String, Integer>> getEntries() {
        return entries.entrySet();
    }

    /**
     * Change the number ordered for a SKU.
     * Can be negative.
     * If the new total is less or equal 0 then the sku is removed from the wish list.
     * the quantity will always be 1
     * when the user clicks remove the quantity changes from 1 to 0, thus removing the item.
     * @param skuId The id of the sku
     */
    public void add(@NonNull String skuId) {
            entries.put(skuId, 1);
        }



    public void clear(@NonNull String skuId) {
        entries.remove(skuId);
    }

}
