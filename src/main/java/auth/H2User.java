package auth;

import lombok.NonNull;
import util.H2Base;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings({"unused"})
public class H2User extends H2Base implements IUserLogin, AutoCloseable {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(H2User.class);

    public enum DB {
        MEMORY("jdbc:h2:mem:test"),
        FILE("\"jdbc:h2:~/test\"");
        private String file;
        DB(String file) {
            this.file = file;
        }
        String file() {
            return file;
        }
    }

    public H2User(DB db) {
        super(db.file());
        try {
            initTable();
        } catch (Exception e) {
            LOG.error("Can't find database driver: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void initTable() throws SQLException {
        try (PreparedStatement ps = prepare("CREATE TABLE IF NOT EXISTS users (name VARCHAR(255) PRIMARY KEY, hash VARCHAR(255))")) {
            ps.execute();
        }
    }

    @Override
    public synchronized boolean login(@NonNull final String userName, @NonNull final String password) {
       //errIfClosed();
        try {
            return loginSQL(userName, password);
        } catch (SQLException e) {
            LOG.error("Can't login " + userName + ": " + e.getMessage());
            return false;
        }
    }

    @Override
    public synchronized boolean register(@NonNull final String userName, @NonNull final String password) {
       // errIfClosed();
        try {
            return registerSQL(userName, password);
        } catch (SQLException e) {
            LOG.error("Can't register " + userName + ": " + e.getMessage());
            return false;
        }
    }

    private boolean loginSQL(String userName, String password) throws SQLException {
        try (PreparedStatement ps = prepare("SELECT hash FROM users WHERE name = ?")) {
            ps.setString(1, userName);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String hash = rs.getString("hash");
                return hash != null && validate(password, hash);
            }
        }
        return false;
    }

    private boolean registerSQL(String userName, String password) throws SQLException {
        String hash = hash(password);
        if (hash == null) {
            return false;
        }
        if (hasUserSQL(userName)) {
            return false;
        }
        String query = "INSERT into users (name, hash) VALUES(?,?)";
        try (PreparedStatement ps = prepare(query)) {
            ps.setString(1, userName);
            ps.setString(2, hash);
            int count = ps.executeUpdate();
            LOG.debug("insert count = " + count);
            return count == 1;
        }
    }


    private boolean hasUserSQL(String userName) throws SQLException {
        try (PreparedStatement ps = prepare("SELECT name FROM users WHERE name = ?")) {
            ps.setString(1, userName);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        }
    }

    private boolean validate(String password, String hash) {
        try {
            return PasswordHash.validatePassword(password, hash);
        } catch (NoSuchAlgorithmException |InvalidKeySpecException e) {
            LOG.error("Can't validate password: " + e.getMessage());
            return false;
        }
    }

    private String hash(String password) {
        try {
            return PasswordHash.createHash(password);
        } catch (NoSuchAlgorithmException |InvalidKeySpecException e) {
            LOG.error("Can't hash password <" + password + ">: " + e.getMessage());
            return null;
        }
    }

}
